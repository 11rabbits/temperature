component extends="lib.framework.one" output="false" {
	this.applicationTimeout = createTimeSpan(0, 2, 0, 0);
	this.name = "family_temperature";
	this.datasource = "temperatureDB";
	this.setClientCookies = true;
	this.sessionManagement = true;
	this.sessionTimeout = createTimeSpan(0, 0, 30, 0);
	this.localmode="modern";
	this.ormenabled=true;
	this.ormsettings = {
		dbcreate = "dropcreate",
		dialect = "PostgreSQL",
		flushAtRequestEnd = false
	};
	this.mappings = {
		//"/logbox" = expandPath( "./subsystems/logbox" ),
		"/rulebox" = expandPath( "./subsystems/rulebox" )
	};


	// FW/1 settings
	variables.framework = {
		action = 'action',
		defaultSection = 'main',
		defaultItem = 'default',
		error: "main.error",
		generateSES = false,
		SESOmitIndex = true,
		diEngine = "di1",
		diComponent = "lib.framework.ioc",
		diLocations = "model, controllers",
		diConfig = {
			transients: [ "rules" ],
			loadListener: ( di1 ) => {
				// Create an impersonation of WireBox :D
				di1.declare( "WireBox" ).asValue({
					getInstance: ( name, initArguments ) => {
						// Parse object@module to get subsystem
						var module = name.listToArray( "@" ).last();
						return getBeanFactory( module ).getBean( name, initArguments );
					}
				});
				// Pull in the root logger as a core citizen
				//di1.declare( "Logger" ).asValue( getBeanFactory( "logbox" ).getBean( "LogBox" ).getRootLogger() );
			}
		},
		subsystems: {
			//logbox: {
				//diConfig: {
					//loadListener: ( di1 ) => {
						//di1.declare( "LogBoxConfig" ).instanceOf( "logbox.system.logging.config.LogBoxConfig" )
						//.withOverrides( { CFCConfigPath: "conf.LogBoxConfig" } )
						//.done()
						//.declare( "LogBox" ).instanceOf( "logbox.system.logging.LogBox" )
						//.withOverrides( { config: di1.getBean( "LogBoxConfig" ) } );
					//}
				//}
			//},
			rulebox: {
				diLocations: [ "/models" ],
				diConfig: {
					// Because FW/1 assumes objects as singletons by default,
					// we must define transients outside of convention
					transientPattern: "^(Rule|Result)",
					// Alias the RuleBox CFCs to match how WireBox expects them
					singulars: { models: "@rulebox" },
					loadListener: ( di1 ) => {
						// Because RuleBox's Builder CFC lacks the accessors annotation,
						// DI/1 will not inject its expected dependencies so we'll declare a custom instance
						di1.declare( "Builder@rulebox" ).asValue({
							ruleBook: ( name ) => {
								return di1.getBean( "RuleBook", { name: name } );
							},
							rule: ( name ) => {
								return di1.getBean( "Rule", { name: name } );
							}
						});
					}
				}
			}
		},
		reload = 'reload',
		reloadApplicationOnEveryRequest = true,
		routes = [ ],
		trace: false,
	};

	public void function setupSession() {  }

	public void function setupRequest() {  }

	public void function setupView() {  }

	public void function setupResponse() {  }

	public string function onMissingView(struct rc = {}) {
		dump(rc);
abort;
		return "Error 404 - Page not found.";
	}
}
