component name="main" output=false accessors=true {

	property fw;
	property groupService;
	property securityService;

	public void function default(struct rc = {}) {

	}

	public void function login(struct rc = {}){

	}

	public void function register(struct rc = {}){

	}

	public void function doLogin(struct rc = {}){
		result = false;
		if(rc.keyExists('email') && rc.keyExists('password')){
			validLogin = securityService.validateLogin(rc.email, securityService.encryptUserPassword(rc.password));
			if(validLogin.len()) {
				echo(validLogin);
				createSession(validLogin);
			}else{
				echo('no user found');
			}
		}else{
			rc.msg = 'Username and Password are required';
			fw.redirect(action='main.login');
		}
		return result;
	}

	public void function addUser(required struct rc){
		newUser = entityNew('User');
		newUser.setEmail(rc.email);
		newUser.setPassword(securityService.encryptUserPassword(rc.password));
		newUser.setDisplayName(rc.name);
		newUser.setActive(false);
		newUser.setAdmin(false);
		entitySave(newUser);
		ORMFLUSH();
		dump(local);
		dump(rc);
		abort;
	}

	public void function createSession(required string userid){
		cflogin(idletimeout=3600, applicationtoken='#application.name#', cookiedomain='family-temperature.com') {
			cfloginuser(name="#res.other.email#", password="#res.access_token#", roles="user");
		}
	}

	public void function map(struct rc = {}){
		rc.mapdata = {};
		rc.groups = groupService.getGroups();
		dump(rc);
	}
}
