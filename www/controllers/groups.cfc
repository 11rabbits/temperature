component accessors=true{

	property groupService;

	public void function default(struct rc = {}){

	}

	public void function list(struct rc = {}){
		rc.groups = groupService.getGroups();
	}

}
