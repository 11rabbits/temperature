component accessors=true{

	public string function encryptUserPassword(required string password){
		result = hash(encrypt(arguments.password, 'local'), "SHA-256", "UTF-8");
		return result;
	}

	public string function validateLogin(required string email, required string password){
		result = '';
		result = queryExecute(
			"select id from tbl_users where email = :e and password = :p",
		{
			e = {value = arguments.email, cfsqltype = "cf_sql_varchar"},
			p = {value = arguments.password, cfsqltype = "cf_sql_varchar"}
		}
		).id;
		return result;
	}

}
