component accessors=true {
	property unitsDAO;

	/**
	* Get a list of all Temperature Units in the system
	*/
	public array function getAllUnits(){
		result = [];
		var units = unitsDAO.getAllUnits();
		result = units;
		return result;
	}

	public UnitsOfMeasure function getUnitByID(required any id){
		result = unitsDAO.getUnitByID(arguments.id);
		return result;
	}

	public any function newUnit(){
		return entityNew('UnitsOfMeasure');
	}

	public void function updateUnit(required numeric id, required string name, required string symbol){
		unitsDAO.updateUnit(arguments.id, arguments.name, arguments.symbol);
	}

}
