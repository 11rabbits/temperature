component accessors=true output=false {

	property groupDAO;

	public void function addGroup(required string name, required boolean isPublic){
		groupDAO.addGroup(name, isPublic);
	}

	public Group function getGroupByID(required string id){
		result = groupDAO.getGroupByID(arguments.id);
		return result;
	}

	public Group function newGroup(){
		result = groupDAO.getNewGroup();
		return result;
	}

	/* TODO - Add scoping for pagination */
	public array function getAllGroups(boolean isPublic=true){
		var result = [];
		if (arguments.isPublic){
			result = entityload('Group', {public=true});
		}else{
			result = entityload('Group');
		}
		return result;
	}

	public array function getGroups(string userID=''){
		result = [];
		if(len(arguments.userID)){
			result = groupDAO.getUserGroups(arguments.userID);
		}else{
			result = groupDAO.getAllPublicGroups();
		}
		return result;
	}

	public struct function getGroupCounts(){
		var result = {public=0, private=0};
		result = groupDAO.getGroupCounts();
		return result;
	}

	public any function updateGroup(required id, required string name, required boolean isPublic){
		groupDAO.updateGroup(arguments.id, arguments.name, arguments.isPublic);
	}
}
