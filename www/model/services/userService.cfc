component accessors=true output=false {

	property userDAO;

	public numeric function countUsers(){
		var result = userDAO.countUsers();
		return result;
	}

	public array function getUsers(){
		var result = userDAO.getUsers();
		return result;
	}

	public User function getUserByID(required string id){
		result = userDAO.getUserByID(arguments.id);
		return result;
	}

	public User function newUser(){
		result = userDAO.getNewUser();
		return result;
	}

}
