component accessors=true output=false persistent=true table="tbl_users"{

// identifier
	property name="id" fieldtype="id" setter=false column="id" generator="uuid";

	property name="email" type="string" hint="E-Mail Address";
	property name="password" type="string" hint="Account Password";
	property name="displayName" type="string" hint="User Display Name";
	property name="color" type="string" hint="Preferred Chart Color";
	property name="admin" type="boolean" hint="Is the user an admin" default=false;
	property name="active" type="boolean" hint="Is the user active" default=true;

	// one User can belong to Many Groups
	property name="groups" fieldtype="one-to-many" cfc="Group" fkcolumn="groupID" singularname="group";


}
