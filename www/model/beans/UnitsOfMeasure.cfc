component accessors=true output=false persistent=true table="lku_units_of_measurement"{

	// identifier
	property name="id" fieldtype="id" setter=false column="id" generator="native";

	property name="name" type="string" length="255";
	property name="symbol" type="string" length="8";

}
