component accessors=true output=false persistent=true table="tbl_groups"{

	// identifier
	property name="id" fieldtype="id" setter=false column="id" generator="uuid";

	property name="name" type="string" hint="Name of the specific group";
	property name="public" type="boolean" hint="Is this group public or private" default=false;

	// one Group can have many Users
	property name="users" fieldtype="one-to-many" cfc="User" fkcolumn="groupID" singularname="user";

}
