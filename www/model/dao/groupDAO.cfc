component accessors=true {

	public void function addGroup(required string name, required boolean isPublic){
		var g = entityNew('Group');
		g.setName(arguments.name);
		g.setPublic(arguments.isPublic);
		entitySave(g);
		ormFlush();
	}

	public Group function getGroupByID(required string id) {
		result = entityLoadByPK('Group', arguments.id);
		return result;
	}

	public Group function getNewGroup(){
		result = entityNew('Group');
		return result;
	}

	public array function getUserGroups(required string id){
		result = [];
		result = entityLoadByPK('Group', arguments.id);
		return result;
	}

	public array function getAllPublicGroups(){
		result = [];
			result = entityLoad('Group');
		return result;
	}

	public struct function getGroupCounts(){
		var result = {public=0, private=0};
		result['public'] = getCountOfGroups(true);
		result['private'] = getCountOfGroups(false);
		return result;
	}

	public numeric function getCountOfGroups(boolean isPublic = false){
		result = 0;
		var userCount = queryExecute(
			"select count(*) as groups from tbl_groups where public = :pub",
			{pub = {value = arguments.isPublic, cfsqltype="cf_sql_boolean"}},
			{}
		);
		if(userCount.recordcount){
			result = usercount.groups;
		}
		return result;
	}

	public void function updateGroup(required id, required string name, required boolean isPublic){
		if(arguments.id){
			var group = entityLoadByPK('Group', arguments.id);
			group.setName(arguments.name);
			group.setPublic(arguments.isPublic);
		}else{
			var group = entityNew('Group');
			group.setName(arguments.name);
			group.setPublic(arguments.isPublic);
		}
		entitySave(local.group);
		ormFlush();
	}

}
