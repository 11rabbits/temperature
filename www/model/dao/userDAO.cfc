component accessors=true {

	public numeric function countUsers(){
		var result = 0;
		var qry = queryExecute("select count(id) as users from tbl_users");
		if(qry.recordcount){
			return qry.users;
		}
		return result;
	}

	public array function getUsers(){
		return EntityLoad('User');
	}

	public User function getNewUser(){
		return EntityNew('User');
	}

}
