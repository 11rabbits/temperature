component accessors=true{

	public array function getAllUnits(){
		result = entityload('UnitsOfMeasure');
		return result;
	}

	public array function getActiveUnits(){

	}

	public UnitsOfMeasure function getUnitByID(required numeric id){
		result = entityLoadByPK('UnitsOfMeasure', arguments.id);
		return result;
	}

	public void function updateUnit(required numeric id, required string name, required string symbol){
		if(arguments.id){
			var uom = entityLoadByPK('UnitsOfMeasure', arguments.id);
			local.uom.setName(arguments.name);
			local.uom.setSymbol(arguments.symbol);
		}else{
			var uom = entityNew('UnitsOfMeasure');
			local.uom.setName(arguments.name);
			local.uom.setSymbol(arguments.symbol);
		}
		entitySave(local.uom);
		ormFlush();
	}


}
