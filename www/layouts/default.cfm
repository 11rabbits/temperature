<!DOCTYPE html>
<html>
	<head>
		<title>Family Temperature</title>
	</head>
	<body>
		<!--- Includes for CSS and JS --->
		<cfinclude template="/layouts/templates/header/header.cfm">
		<cfoutput>
			<cfinclude template="/layouts/templates/common/navbar.cfm">
			<main>
				<div class="container">
					#body#
				</div>
			</main>
		</cfoutput>
		<!--- FOOTER --->
		<cfinclude template="/layouts/templates/footer/footer.cfm">
		<!--- /FOOTER --->
	</body>
	<!-- JavaScript and dependencies -->
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js" integrity="sha384-oesi62hOLfzrys4LxRF63OJCXdXDipiYWBnvTl9Y9/TRlw5xlKIEHpNyvvDShgf/" crossorigin="anonymous"></script>
</html>
