<footer class="footer mt-5 py-3 bg-dark">
	<cfinclude template="/layouts/templates/footer/footer_text.cfm">
</footer>
<cfinclude template="/layouts/templates/footer/footer_js.cfm">
