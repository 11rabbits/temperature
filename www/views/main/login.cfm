<cfoutput>
	<cfif rc.keyExists('msg')>
		<div class="alert alert-danger">
			#rc.msg#
		</div>
	</cfif>
	<div class="row">
		<div class="col-8">
			<h2>Login</h2>
			<div class="card border-primary">
				<div class="card-header bg-primary text-white">Login</div>
				<div class="card-body">
					<form action="#buildURL('main.doLogin')#" method="POST">
						<div class="mb-3">
							<label for="email" class="form-label">Email address</label>
							<input type="email" class="form-control" id="email" name="email">
						</div>
						<div class="mb-3">
							<label for="password" class="form-label">Password</label>
							<input type="password" class="form-control" id="password" name="password">
						</div>
						<button type="submit" class="btn btn-primary">Submit</button>
					</form>
				</div>
			</div>
		</div>
		<div class="col-4">
			<h2>Register</h2>
			<p>Registering for this site allows you to record <em><u>your</u></em> information in a private manner.  It allows us to aggregate the data you provide to help other people in your community see information that can help them make health decisions.</p>
			<a href="#buildURL(':main.register')#">
				<button class="btn btn-primary">Register</button>
			</a>
		</div>
	</div>
	<cfdump var="#rc#">
</cfoutput>
