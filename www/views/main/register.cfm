<cfoutput>
	<div class="card border-info">
		<div class="card-header bg-info text-white">Register</div>
			<div class="card-body">
				<form action="#buildURL('main.addUser')#" method="POST">
					<div class="mb-3">
						<label for="regEmail" class="form-label">Email address</label>
						<input type="email" class="form-control" id="regEmail" name="email" aria-describedby="emailHelp">
						<div id="emailHelp" class="form-text">Your email will be encrypted to ensure privacy.</div>
					</div>
					<div class="mb-3">
						<label for="regPass" class="form-label">Password</label>
						<input type="password" class="form-control" id="regPass" name="password" aria-describedby="passHelp">
						<div id="passHelp" class="form-text">Your password will be encrypted to ensure privacy.</div>
					</div>
					<div class="mb-3">
						<label for="regName" class="form-label">Display Name</label>
						<input type="text" class="form-control" id="regName" name="name" aria-describedby="nameHelp">
						<div id="nameHelp" class="form-text">This is the name we'll use for your displays.</div>
					</div>
					<button type="submit" class="btn btn-primary">Submit</button>
				</form>
			</div>
		</div>
	</div>
</cfoutput>
