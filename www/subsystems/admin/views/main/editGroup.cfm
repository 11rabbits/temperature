<cfoutput>
	<h2><cfif (len(rc.group.getID()))>Edit<cfelse>Add</cfif> Group</h2>
	<div class="row">
		<div class="col-md-12">
			<form action="#buildURL('admin.updateGroup')#" method="POST">
				<input type="hidden" name="id" value="#rc.group.getID()#">
				<div class="form-group">
					<label for="groupName">Group Name</label>
					<input type="text" class="form-control" id="groupName" name="groupName" placeholder="Group Name" value="#rc.group.getName()#">
				</div>
				<div class="form-group">
					<div class="form-check">
						<input class="form-check-input" type="checkbox" name="groupPrivate" id="groupPrivate">
						<label class="form-check-label" for="groupPrivate">
							Public Group
						</label>
					</div>
				</div>
				<button type="submit" class="btn btn-primary"><cfif (len(rc.group.getID()))>Update<cfelse>Add</cfif> Group</button>
			</form>
		</div>
	</div>
</cfoutput>
