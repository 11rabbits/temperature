<cfoutput>
	<h2>Groups</h2>
	<table class="table table-bordered table-striped">
		<thead>
			<tr>
				<th>Account</th>
				<th>Group</th>
				<th>Public</th>
				<th>Members</th>
			</tr>
		</thead>
		<tbody>
			<cfloop array="#rc.groups#" index="group">
				<tr>
					<td></td>
					<td>#group.getName()#</td>
					<td>#group.getPublic()#</td>
					<td>#arrayLen(group.getUsers())#</td>
				</tr>
			</cfloop>
		</tbody>

	</table>
	<cfdump var="#rc#">
</cfoutput>
