<cfoutput>
	<form action="#buildURL('admin.saveUOM')#" method="POST">
		<input type="hidden" name="id" value="#rc.unit.getID()#">
		<div class="form-group">
			<label for="name">Measurement Name:</label>
			<input type="text" class="form-control" id="name" name="name" value="#rc.unit.getName()#" placeholder="Name">
		</div>
		<div class="form-group">
			<label for="symbol">Measurement Symbol:</label>
			<input type="text" class="form-control" id="symbol" name="symbol" value="#rc.unit.getSymbol()#" maxlength="8" placeholder="Symbol">
		</div>
		<button type="submit" class="btn btn-primary">Submit</button>
	</form>
</cfoutput>
