<cfoutput>
	<ul class="nav nav-tabs mb-1" id="adminTabs" role="tablist">
		<li class="nav-item">
			<a class="nav-link bg-danger active text-white" id="home-tab" data-toggle="tab" href="##home" role="tab" aria-controls="home" aria-selected="true">Dashboard</a>
		</li>
		<li class="nav-item">
			<a class="nav-link bg-primary text-white" id="profile-tab" data-toggle="tab" href="##users" role="tab" aria-controls="profile" aria-selected="false">Users/Groups</a>
		</li>
		<li class="nav-item">
			<a class="nav-link bg-success text-white" id="contact-tab" data-toggle="tab" href="##ref" role="tab" aria-controls="ref" aria-selected="false">Reference</a>
		</li>
		<li class="nav-item">
			<a class="nav-link bg-info text-white" id="contact-tab" data-toggle="tab" href="##tools" role="tab" aria-controls="tools" aria-selected="false">Tools</a>
		</li>
	</ul>
	<div class="tab-content" id="myTabContent">
		<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
			<div class="row">
				<div class="col-4">
					<div class="card border-danger">
						<div class="card-header bg-danger">Current Avg. Temp.</div>
						<div class="card-body"><h1>temp average goes here</h1></div>
					</div>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="users" role="tabpanel" aria-labelledby="users-tab">
			<div class="row">
				<div class="col-6">
					<div class="card border-primary">
						<div class="card-header bg-primary text-white">Users Count</div>
						<div class="card-body">
							<h1>#rc.userCount#</h1>
						</div>
						<div class="card-footer">
							<div>
								<a href="#buildURL('main.listUsers')#">View Users</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-6">
					<div class="card border-info text-white">
						<div class="card-header bg-info">Group Count</div>
					<div class="card-body">
						<h1>#rc.groups.len()#</h1>
					</div>
					<div class="card-footer">
						<div class="col-md-6">
								<a href="#buildURL('main.editGroup')#">Add Group</a>
						</div>
						<div>
								<a href="#buildURL('main.listGroups')#">View Groups</a>
						</div>
					</div>
				</div>
					</div>
			</div>
		</div>
		<div class="tab-pane fade" id="ref" role="tabpanel" aria-labelledby="contact-tab">
			<div class="row">
				<div class="col-6">
					<div class="card border-success">
						<div class="card-header bg-success text-white">Temperature Units</div>
						<div class="card-body">
							<table class="table table-bordered">
								<thead>
									<tr>
										<th>Name</th>
										<th>Symbol</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<cfloop array="#rc.units#" index="unit">
										<tr>
											<td>#unit.getName()#</td>
											<td>#unit.getSymbol()#</td>
											<td><a href="#buildURL('main.editUnitOfMeasure')#&id=#unit.getID()#">edit</a></td>
										</tr>
									</cfloop>
								</tbody>
							</table>
						</div>
						<div class="card-footer">
						<div class="col-md-6">
							<a href="#buildURL('main.editUnitOfMeasure')#">Add Units</a>
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>
		<div class="tab-pane fade" id="tools" role="tabpanel" aria-labelledby="contact-tab">
			<div class="row">
				<div class="col-12">
					<div class="card border-info">
						<div class="card-header bg-info text-white">Tools</div>
						<div class="card-body">
							<ul>
								<li><a href="#buildURL('admin:main.loadData')#&data=temp">Add default temperatures</a></li>
								<li><a href="#buildURL('admin:main.loadData')#&data=users">Add default users</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>

	</div>
</cfoutput>
