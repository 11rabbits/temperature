<cfoutput>
	<h2>Users</h2>
	<table class="table table-bordered table-striped">
		<thead>
			<tr>
				<th>Active</th>
				<th>E-Mail</th>
				<th>Password</th>
				<th>Display Name</th>
				<th>Prefered Color</th>
				<th>Admin</th>
			</tr>
		</thead>
		<tbody>
	<cftry>
			<cfloop array="#rc.users#" index="user">
				<tr>
					<td>#yesNoFormat(user.getActive())#</td>
					<td>#user.getEmail()#</td>
					<td>#hash(user.getPassword())#</td>
					<td>#user.getDisplayName()#</td>
					<td>#user.getColor()#</td>
					<td>#yesNoFormat(user.getAdmin())#</td>
				</tr>
			</cfloop>
	<cfcatch>
		<cfscript>
			dump(rc);
			abort;
		</cfscript>
	</cfcatch>
	</cftry>
		</tbody>
	</table>
</cfoutput>
