component name="admin" accessors=true output=false {
	property fw;
	property groupService;
	property unitsService;
	property userService;

/* VIEWS */
	/**
	* General dashboard view
	*/
	public void function default(struct rc = {}) {
		rc.userCount = userService.countUsers();
		rc.units = unitsService.getAllUnits();
		rc.groups = groupService.getGroups();
	}

	public void function listUsers(struct rc = {}){
		rc.users = userService.getUsers();
	}

	public void function listGroups(struct rc = {}){
		rc.groups = groupService.getAllGroups(false);
	}


/* FORMS */
	public void function editGroup(struct rc = {}){
		if(rc.keyExists('id')){
			rc.group = groupService.getGroupByID(rc.id);
		}else{
			rc.group = groupService.newGroup();
		}
	}

	public void function editUser(struct rc = {}){
		if(rc.keyExists('id')){
			rc.user = userService.getUserByID(rc.id);
		}else{
			rc.user = userService.newUser();
		}
	}

	public void function editUnitOfMeasure(struct rc={}){
		if(rc.keyExists('id')){
			rc.unit = unitsService.getUnitByID(rc.id);
		}else{
			rc.unit = unitsService.newUnit();
		}
	}


/* ACTIONS */
	public void function updateGroup(required struct rc){
		var isPublic = rc.keyExists('groupPrivate')?true:false;
		var exists = rc.keyExists('id') && len(rc.id) ? rc.id : 0;
		groupService.updateGroup(local.exists, rc.groupname, local.isPublic);
		fw.redirect( action = 'admin.default');
	}

	/*
		Save a Unit Of Measure
	*/
	public void function saveUOM(struct rc={}){
		var exists = rc.keyExists('id') && len(rc.id) ? rc.id : 0;
		unitsService.updateUnit(local.exists, rc.name, rc.symbol);
		fw.redirect( action = 'admin.default');
	}

	/****** TOOLS ************/
	public void function loadData(required struct rc){
		if(rc.keyExists('data')){
			switch(rc.data){
				case 'temp': {
					unitsService.updateUnit(0, 'Celsius', 'C');
					unitsService.updateUnit(0, 'Fahrenheit', 'F');
					break;
				}
				case 'users': {
					break;
				}
			}
		}
		fw.redirect('admin:main');
	}
}
