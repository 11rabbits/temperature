# Temperature

This is a starting app to be used for tracking a family or group's medical metrics.

## Docker
This is designed to run ion a dockerized environment.  To start the application, change to the `docker/compose` directory then run `docker-compose up` to start.
* CF Environment: http://localhost:8080
* PostgreSQL: http://localhost:5432
